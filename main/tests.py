from django.test import LiveServerTestCase, TestCase, tag
from django.urls import reverse
from django.urls.base import resolve
from selenium import webdriver
from .views import home, kucing


@tag('functional')
class FunctionalTestCase(LiveServerTestCase):
    """Base class for functional test cases with selenium."""

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        # Change to another webdriver if desired (and update CI accordingly).
        options = webdriver.chrome.options.Options()
        # These options are needed for CI with Chromium.
        options.headless = True  # Disable GUI.
        options.add_argument('--no-sandbox')
        options.add_argument('--disable-dev-shm-usage')
        cls.selenium = webdriver.Chrome(options=options)

    @classmethod
    def tearDownClass(cls):
        cls.selenium.quit()
        super().tearDownClass()


class MainTestCase(TestCase):
    def test_root_url_status_200(self):
        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)
        # You can also use path names instead of explicit paths.
        response = self.client.get(reverse('main:home'))
        self.assertEqual(response.status_code, 200)

    def test_kucing_url_status_200(self):
        response = self.client.get(reverse('main:kucing'))
        self.assertEqual(response.status_code, 200)

class UrlsTest(TestCase):

    def setUp(self):
        self.homeUrl = reverse('main:home')
        self.kucingUrl = reverse('main:kucing')

    def test_home_use_correct_funct(self):
        found = resolve(self.homeUrl)
        self.assertEqual(found.func, home)
    
    def test_kucing_use_correct_funct(self):
        found = resolve(self.kucingUrl)
        self.assertEqual(found.func, kucing)


class MainFunctionalTestCase(FunctionalTestCase):
    def test_root_url_exists(self):
        self.selenium.get(f'{self.live_server_url}/')
        html = self.selenium.find_element_by_tag_name('html')
        self.assertNotIn('not found', html.text.lower())
        self.assertNotIn('error', html.text.lower())
