from django.shortcuts import render


def home(request):
    return render(request, 'main/index.html')

def kucing(request):
    return render(request, 'main/kucing.html')
